#/bin/bash

handyMeld() {
    meld $1$3 $2$3
}
alias mel=handyMeld

function cd {
    if [ $# -eq 0 ]; then
        pushd ~ > /dev/null
    elif [ " $1" = " -" ]; then
        pushd "$OLDPWD" > /dev/null
    else
        pushd "$@" > /dev/null
    fi
}

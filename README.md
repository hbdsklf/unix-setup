# README #

**Please note that this is not more than a draft yet!**

### What is this repository for? ###

The idea is to collect config files and create a setup script for Unix/Linux/Debian (full Unix compatibility would be nice, ideally the setup script would detect the OS. For now I will focus on Debian-based systems).

### How do I get set up? ###

After your operating system is installed you can simply issue the following command:
```
#!sh
cd ~/ && wget https://bitbucket.org/hbdsklf/unix-setup/raw/master/setup.sh && chmod +x setup.sh && ./setup.sh

```
**Please note that the above is a draft and is not working yet!**

### Thanks to ###
Special thanks to to the owners of the following repositories:

* https://github.com/ReekenX/phpcheck-git
* https://github.com/fearside/ProgressBar

And to TD for his teachings.

### Contribution ###

Any contributions, including improvement ideas, are welcome! You may also [create an issue](https://bitbucket.org/hbdsklf/unix-setup/issues/new).
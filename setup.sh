echo "Updating system"
sudo apt update
sudo apt upgrade

echo "Installing base packages"
sudo apt install vim screen openssh-server git vlc exuberant-ctags

echo "Installing web development packages"
sudo apt install phpmyadmin mysql-server

echo "GUI tools"
sudo apt install meld chromium-browser filezilla mysql-workbench shutter

echo "Congiguring command line tools"
git clone https://bitbucket.org/hbdsklf/unix-setup.git ~/
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

echo "Configuring GIT"
#TODO: Configure MELD
#TODO: CTags hook -> ctags -R --tag-relative=yes --exclude=.git --exclude=themes --exclude=update --exclude=installer --exclude=lib/PHPUnit -f ./.git/tags .
git config --global init.templatedir '~/.git-templates' && cd ~/.git-templates/hooks && chmod a+x pre-commit && cd -

echo "Cleaning up"
sudo apt autoremove
sudo apt autoclean
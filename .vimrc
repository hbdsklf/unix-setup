" This .vimrc requires docker to work properly
" TODO: add shellcheck as makeprg for filetype "sh"

set nocompatible              " be iMproved, required
"filetype off                  " required, TODO: what for? Maybe this was used by Vundle

" First time setup of vim-plug, plugin manager
" This needs to be done before any plugin is configured!
if empty(glob('~/.vim/autoload/plug.vim')) " check if plug.vim exists, if not:
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC " install configured plugins and reload this file
endif


" Load plugins via vim-plug (https://github.com/junegunn/vim-plug)
call plug#begin('~/.vim/plugged')
    Plug 'aklt/plantuml-syntax' " Provides syntax highlighting for PlantUML
    Plug 'vim-latex/vim-latex' " Adds support for LaTex
    Plug 'tpope/vim-fugitive' " GIT wrapper, also sets ctags directory to .git
    Plug 'rstacruz/sparkup', {'rtp': 'vim/'} " lets you write HTML in a CSS manner

    Plug 'tobyS/vmustache' " template engine for vimscript, required by tobyS/pdv
    Plug 'SirVer/ultisnips' " snippet manager, required by tobyS/pdv
    Plug 'tobyS/pdv' " PHP Documentor for VIM

    "Plug 'w0rp/ale' " TODO: is this necessary
    "Plug 'majutsushi/tagbar' " Tagbar, TODO: is this necessary

    " deoplete, gives error due to problems with vim-hug-neovim-rpc / python:
    "if has('nvim')
    "  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    "else
    "  Plug 'Shougo/deoplete.nvim'
    "  Plug 'roxma/nvim-yarp'
    "  Plug 'roxma/vim-hug-neovim-rpc'
    ""endif
    "let g:deoplete#enable_at_startup = 1
call plug#end()

colorscheme peachpuff " use the color scheme 'peachpuff'
filetype on " turn on filetype detection, TODO: might conflict with plugins
syntax on " enable syntax highlighting
let php_folding=0
let php_sql_query=1
let php_htmlInStrings = 1
let php_baselib = 1
let php_fold_arrays = 1
let php_fold_heredoc = 1
let xhtml_no_embedded_mathml = 0
let xhtml_no_embedded_svg = 0
set colorcolumn=80 " According to PSR-2: lines should be 80 characters or less
set textwidth=120 " According to PSR-2: soft-limit at 120
set ignorecase smartcase " search case insensitive if string is lowercase only
set tabstop=4 " a tab is displayed as 4 characters wide
set shiftwidth=4 " automatic indent is 4 spaces wide
set expandtab " insert spaces instead of tabs
set cindent " automatically indent based on C-style source
set hlsearch " highlight search results
set mouse=a " enable mouse <a>lways
set ttymouse=sgr " set terminal type for mouse handling. See :help ttymouse
set number " show line numbers
set incsearch " already search while typing
set statusline=[%-n]%F%<%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L] " configure status line
set laststatus=2 " always display status line
set wildmenu " use a menu to complete commands
set wildmode=longest:full,full " first <TAB> completes to longest common string and invokes wildmenu. Further <TAB> will cycle.
set nospell spelllang=en_us,de_ch " According to coding standards
nnoremap <F2> :setlocal spell! spell?<CR>
autocmd FileType gitcommit setlocal colorcolumn=70 " according to commit guidelines
autocmd FileType gitcommit setlocal textwidth=70 " according to commit guidelines
autocmd FileType latex setlocal spell makeprg=autolatex
autocmd FileType plaintex setlocal spell makeprg=autolatex
autocmd FileType tex setlocal spell makeprg=autolatex
":autocmd BufWritePre * %perldo s/\s*$//
let g:pdv_template_dir = $HOME ."/.vim/plugged/pdv/templates_snip"
nnoremap <C-p> :call pdv#DocumentWithSnip()<CR>
"Bundle 'majutsushi/tagbar' " Tag-sidebar

set diffexpr=MyDiff()
function MyDiff()
   let opt = ""
   if &diffopt =~ "icase"
     let opt = opt . "-i "
   endif
   if &diffopt =~ "iwhite"
     let opt = opt . "-b "
   endif
   silent execute "!diff -a --binary " . opt . v:fname_in . " " . v:fname_new .
    \  " > " . v:fname_out
endfunction
" Do compile LaTex as PDF, not as DVI
let g:Tex_DefaultTargetFormat = "pdf"

" Use "very magic" RegEx escaping. See help :magic
map / /\v

if has("gui_running")
  " GUI is running or is about to start. Maximize gvim window.
  set lines=999 columns=999
endif


" Filetype specific mappings:
au FileType yaml setlocal tabstop=2 shiftwidth=2 " set tab with to 2 spaces for YAML files

" This makes all occurences of the word that is currently under the cursor bold
" From https://stackoverflow.com/a/25233145
highlight WordUnderCursor cterm=bold gui=bold
set updatetime=10
function! HighlightWordUnderCursor()
    if getline(".")[col(".")-1] !~# '[[:punct:][:blank:]]'
        exec 'match' 'WordUnderCursor' '/\V\<'.expand('<cword>').'\>/'
    else
        match none
    endif
endfunction
autocmd! CursorHold,CursorHoldI * call HighlightWordUnderCursor()

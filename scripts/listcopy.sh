#/bin/bash

if [[ "$1" == "" ]] || [[ "$2" == "" ]] || [[ "$3" == "" ]]; then
    echo "Usage: ./listcopy.sh <fileList> <fromDirectory> <toDirectory>"
    exit
fi

while read line
do
    mkdir -p `dirname $3$line`
    cp $2$line $3$line
done <$1
#!/bin/bash

total_mem=$(/usr/bin/free -m | /usr/bin/awk '$1 == "Mem:" { print $2 }')
used_mem=$(/usr/bin/free -m | /usr/bin/awk '$1 == "Mem:" { print $3 }')

echo $(( used_mem * 100 / total_mem ))%

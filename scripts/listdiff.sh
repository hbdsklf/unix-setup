#/bin/bash

if [[ "$1" == "" ]] || [[ "$2" == "" ]] || [[ "$3" == "" ]]; then
    echo "Usage: ./listdiff.sh <fileList> <fromDirectory> <toDirectory>"
    exit
fi;

while read line
do
    meld $2$line $3$line
done <$1

#/bin/bash

# example usage:
# ./git-file-list.sh 27a15a3 f7b93f8 6372b98 e731aeb 7d081a1 9f6c54c 8c8053e | xclip -sel clip

OUT=""
GITBUF=""

if [ "$#" -lt 1 ]; then
    echo "Usage git-file-list <commit-id>( <commit-id>(...))"
    exit
fi

# foreach commit id
for revision in "$@"
do
    GITBUF=`git diff-tree --no-commit-id --name-only -r $revision`
    OUT="$OUT
$GITBUF"
done

echo "$OUT" | sort -u -V
